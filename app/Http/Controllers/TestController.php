<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function store(Request $request)
    {
        //LOGIC HERE
    	$request->validate([
	        'name' => 'required|string|max:255',
	    ]);

    	$user = new UserTest;
    	$user->name = $request->name;
    	$user->save();

    	// if displayed ID start from 1
        // $user->parity = $user->id % 2 === 0 ? 'Even' : 'Odd';
        // if displayed ID start from 0
        $user->parity = $user->id % 2 === 1 ? 'Even' : 'Odd';
        $user->save();

        return redirect()->route('index');
    }

}
